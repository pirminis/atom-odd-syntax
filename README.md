<<<<<<< HEAD
# odd-syntax theme

This is a light syntax theme for Atom.

The languages that were definately touched were: Ruby, HAML, Twig, PHP, JavaScript, YAML.

This syntax theme has 4 main colors:
 - __red__ for strings and numbers
 - __blue__ for keywords
 - __teal__ for operators, separators, punctuation and constants
 - __black__ for almost everything else
 - comments are _grey_

![A screenshot of your theme](https://f.cloud.github.com/assets/69169/2289498/4c3cb0ec-a009-11e3-8dbd-077ee11741e5.gif)
